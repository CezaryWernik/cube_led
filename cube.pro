#-------------------------------------------------
#
# Project created by QtCreator 2016-12-16T13:55:45
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = cube
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui


DESTDIR     = builds
OBJECTS_DIR = builds/objects
MOC_DIR     = builds/mocs
